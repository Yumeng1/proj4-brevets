# Project 4: Brevet time calculator with Ajax

Author: Yumeng Wang
Email: ywang17@uoregon.edu

The brevet rules:
the basic rule is: the open time will use the maxium speed to calculate, and the close time will use the minimum speed to calculate

for example on the website:
if I want to calculate the open time controls at 60km, 120km and suppose the start date is 2019/1/1 00:00:00
I suppose to use the maximum speed shown in the form which indicate that the speed is 34km/h

then the open time at 60km will be:
	60 / 34 =  1.76
which is means 1 hour, to get the minute I need to use the (1.76 - 1) * 60 to get the minutes
	(1.76 - 1) * 60 = 45.6 
which is 46 min. so the open time should be 2019/1/1 01:46:00

and about the close time will be in the same logic:
	60 / 15 = 4
which means 4 hour. so that the close time should be 2019/1/1 04:00:00

Moreover, if there's longer trip
for example on the website:
if I want to calculate the open time controls at 890km in a 1000km brever and suppose the start date is 2019/1/1 00:00:00

firstly, consider the first 200km according to the form the max speed is 34km/h

then plus the 200-400 range which max speed is 32km/h

then plus the 400-600 range which max speed is 30km/h

last plus the 600-1000 range which max speed is 28km/h

so the open time will be :
(200 / 34) + (200 / 32) + (200 / 30) + (290 / 28) = 29 hour and 9 minutes 