import acp_times
import arrow
import nose


def test_200():
    date = arrow.get('2019-11-11T00:00:00')
    assert acp_times.open_time(100, 200, arrow.get(date)) == date.shift(hours=+2, minutes =+ 56)
    assert acp_times.close_time(100, 200, arrow.get(date)) == date.shift(hours=+6, minutes =+ 40)

def test_400():
    date = arrow.get('2019-11-11T00:00:00')
    assert acp_times.open_time(300, 400, arrow.get(date)) == date.shift(hours=+9, minutes =+ 00)
    assert acp_times.close_time(300, 400, arrow.get(date)) == date.shift(hours=+20, minutes =+ 00)

def test_600():
    date = arrow.get('2019-11-11T00:00:00')
    assert acp_times.open_time(500, 600, arrow.get(date)) == date.shift(hours=+15, minutes=+28)
    assert acp_times.close_time(500, 600 , arrow.get(date)) == date.shift(day=+1, hours=+ 9, minutes =+ 20)

def test_1000():
    date = arrow.get('2019-11-11T00:00:00')
    assert acp_times.open_time(900, 1000, arrow.get(date)) == date.shift(day=+1,hours=+5, minutes=+31)
    assert acp_times.close_time(900, 1000 , arrow.get(date)) == date.shift(day=+2, hours=+ 18, minutes =+ 15)

def test_special():
    date = arrow.get('2019-11-11T00:00:00')
    assert acp_times.open_time(380, 600, arrow.get(date)) == date.shift(hours=+11, minutes=+30)
    assert acp_times.close_time(380, 600 , arrow.get(date)) == date.shift(day=+1, hours=+ 1, minutes =+ 20)

def test_larger_than_1000():
    date = arrow.get('2019-11-11T00:00:00')
    assert acp_times.open_time(1200, 1000, arrow.get(date)) == date.shift(day=+1,hours=+9, minutes=+ 5)
    assert acp_times.close_time(1200, 1000 , arrow.get(date)) == date.shift(day=+3, hours=+ 3)
