"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
   """
   Args:
      control_dist_km:  number, the control distance in kilometers
      brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
      brevet_start_time:  An ISO 8601 format date-time string indicating
         the official start time of the brevet
   Returns:
      An ISO 8601 format date string indicating the control open time.
      This will be in the same time zone as the brevet start time.
   """
   distance  = control_dist_km
   time = 0
   while distance > 0:
      if distance >= 1000:
         if (distance - 1000) < 200 and (distance - 1000) != 0:
            distance = 1000
         elif distance > 1000:
            time = time + (distance - 1000)/26
            distance = 1000
         elif distance == 1000:
            time = time + (400 / 28)
            distance = 600

      elif distance >= 600:
         if (distance - 600) < 120 and (distance - 600) != 0:
            distance = 600  
         elif distance > 600:
            time = time + (distance - 600)/28
            distance = 600
         elif distance == 600:
            time = time + (200 / 30)
            distance = 400
      
      elif distance >=400:
         if (distance - 400) < 80 and (distance - 400) != 0:
            distance = 400
         elif distance > 400:
            time = time + (distance - 400)/30
            distance = 400
         elif distance == 400:
            time = time + (200 / 32)
            distance = 200

      elif distance >= 200:
         if (distance - 200) < 20 and (distance - 200) != 0:
            distance = 200    
         elif distance > 200:
            time = time + (distance - 200)/32
            distance = 200
         elif distance == 200:
            time = time + (200 / 34)  
            distance =0
      
      else:
         time = time + (distance / 34)
         distance = 0
      

   time_hour = int(time)
   time_minute = round((time - time_hour) * 60)
   start_time = arrow.get(brevet_start_time)
   open_time_result = start_time.shift(hours =+time_hour, minutes =+time_minute)    
   return open_time_result.isoformat() 


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
   """
   Args:
      control_dist_km:  number, the control distance in kilometers
         brevet_dist_km: number, the nominal distance of the brevet
         in kilometers, which must be one of 200, 300, 400, 600, or 1000
         (the only official ACP brevet distances)
      brevet_start_time:  An ISO 8601 format date-time string indicating
          the official start time of the brevet
   Returns:
      An ISO 8601 format date string indicating the control close time.
      This will be in the same time zone as the brevet start time.
   """
   time = 0
   distance = control_dist_km

   while distance > 0:
      if distance >= 1000:
         if (distance - 1000) < 200 and (distance - 1000) != 0:
            distance = 1000
         elif distance > 1000:
            time = time + ((distance - 1000)/13.333)
            distance = 1000
         elif distance == 1000:
            time = time + (400 / 11.428)
            distance = 600

      if distance >= 600:
         if (distance - 600) < 120 and (distance - 600) != 0:
            distance = 600  
         elif distance > 600:
            time = time + ((distance - 600)/11.428)
            distance = 600
         elif distance == 600:
            time = time + (200 / 15)
            distance = 400

      if distance >= 400:
         if (distance - 400) < 80 and (distance - 400) != 0:
            distance = 400
         elif distance > 400:
            time = time + ((distance - 400)/15)
            distance = 400
         elif distance == 400:
            time = 200 / 15

      if distance >= 200:
         if (distance - 200) < 20 and (distance - 200) != 0:
            distance = 200    
         elif distance > 200:
            time = time + ((distance - 200)/15)
            distance = 200
         elif distance == 200:
            time = time + (200/15)
            distance =0
      
      else:
         time = time + (distance / 15)
         distance = 0  

   time_hour = int(time)
   time_minute = round((time - time_hour) * 60)
   start_time = arrow.get(brevet_start_time)
   close_time_result = start_time.shift(hours =+time_hour, minutes =+time_minute)
   return close_time_result.isoformat()
